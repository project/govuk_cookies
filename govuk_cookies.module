<?php

/**
 * @file
 * Contains govuk_cookies.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function govuk_cookies_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the govuk_cookies module.
    case 'help.page.govuk_cookies':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides cookie consent forms for cookie pages to allow users to opt-out of non-essential cookies.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function govuk_cookies_theme() {
  $site_name = \Drupal::config('system.site')->get('name');
  $cookie_name = \Drupal::config('govuk_cookies.settings')->get('name');
  $cookie_policy = \Drupal::config('govuk_cookies.settings')->get('types');

  $message = [
    [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'We use some essential cookies to make this service work.',
    ]
  ];
  if (!empty($cookie_policy) && count($cookie_policy) === 1 && current($cookie_policy) === 'analytics') {
    $message[] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'We’d also like to use analytics cookies so we can understand how you use the service and make improvements.',
    ];
  }
  elseif (!empty($cookie_policy)) {
    $message[] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'We’d like to set additional cookies so we can remember your settings, understand how people use the service and make improvements.',
    ];
  }

  return [
    'govuk_cookie_message' => [
      'variables' => [
        'attributes' => [],
        'title' => "Cookies for $site_name",
        'message' => $message,
        'cookie_policy' => $cookie_policy,
        'cookie_name' => $cookie_name,
        'cookie' => false,
        'acceptance_message' => 'govuk-cookies-accepted',
        'rejection_message' => 'govuk-cookies-rejected',
      ],
      'template' => 'govuk-cookie-message',
    ]
  ];
}

/**
 * Implements hook_preprocess_govuk_cookie().
 */
function govuk_cookies_preprocess_govuk_cookie_message(&$variables) {
  $request = \Drupal::request();
  $cookie_name = \Drupal::config('govuk_cookies.settings')->get('name');
  $variables['cookie'] = $request->cookies->get($cookie_name);

  $variables['attributes']['class'] = ['govuk-cookie-banner__message', 'govuk-width-container'];
}
