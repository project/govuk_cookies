<?php

namespace Drupal\govuk_cookies\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure GOVUK cookies settings for this site.
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $currentUser) {
    parent::__construct($config_factory);
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'govuk_cookies_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['govuk_cookies.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('govuk_cookies.settings');

    $form['cookies'] = [
      '#type' => 'details',
      '#title' => $this->t('Cookie settings'),
      '#open' => TRUE,
    ];

    $form['cookies']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie name'),
      '#description' => $this->t('Set the cookie name to record user consent.'),
      '#required' => TRUE,
      '#default_value' => $config->get('name') ?? '_cookie_policy',
    ];

    $form['cookies']['types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Cookie types'),
      '#description' => $this->t('List any non-essential cookies used by this site.'),
      '#options' => [
        'analytics' => 'Analytics',
        'functional' => 'Functional',
        'other' => 'Other'],
      '#required' => TRUE,
      '#default_value' => $config->get('types') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('govuk_cookies.settings');

    $config
      ->set('name', $form_state->getValue('name'))
      ->set('types', array_keys(array_filter($form_state->getValue('types'))))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
